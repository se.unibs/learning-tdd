package it.unibs.ing.se.testing.learningtdd;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class StringCalculatorTest {
    private StringCalculator calculator;

    @Before
    public void init() {
        calculator = new StringCalculator();
    }

    @Test
    public void emptyStringShouldReturnZero() {
        assertThat(calculator.add(""), is(equalTo(0)));
    }

    @Test
    public void nullStringShouldReturnZero() {
        assertThat(calculator.add(null), is(equalTo(0)));
    }

    @Test
    public void singleValue() {
        final int val = 19;
        assertThat(calculator.add(Integer.toString(val)), is(equalTo(val)));
    }

    @Test
    public void multipleValues() {
        assertThat(calculator.add("1 2 3 4 5 6 7 8 9 10"), is(equalTo(55)));
    }

    @Test
    public void multipleValuesWithNewLines() {
        assertThat(calculator.add("1 2 3 4\n5 6 7\n8\n9\n10"), is(equalTo(55)));
    }

    @Test
    public void multipleValuesWithCustomDelimiter() {
        assertThat(calculator.add("//;\n1;2"), is(equalTo(3)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativesNotAllowed() {
        assertThat(calculator.add("1 2 3 4 5 -6 7 8 9 10"), is(equalTo(55)));
    }

}
