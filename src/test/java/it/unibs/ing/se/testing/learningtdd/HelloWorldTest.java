package it.unibs.ing.se.testing.learningtdd;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class HelloWorldTest {
    @Test
    public void shouldReturnHelloWorld() {
        Hello hello = new Hello();
        String result = hello.sayHello();
        assertThat(result, is(equalTo("Hello, World!")));
    }

    @Test
    public void shouldReturnHelloPietro() {
        Hello hello = new Hello();
        String result = hello.sayHelloTo("Pietro");
        assertThat(result, is(equalTo("Hello, Pietro!")));
    }

    @Test
    public void canUseCustomMessage() {
        Hello hello = new Hello("Hola");
        assertThat(hello.sayHelloTo("Pietro"), is(equalTo("Hola, Pietro!")));
    }
}
