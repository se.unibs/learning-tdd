package it.unibs.ing.se.testing.learningtdd;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class GreeterTest {
    @Test
    public void shouldSayHelloWorld() {
        assertThat(new Greeter().sayHello(), is(equalTo("Hello, World!")));
    }
}
