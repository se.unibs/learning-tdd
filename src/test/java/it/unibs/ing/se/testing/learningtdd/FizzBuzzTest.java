package it.unibs.ing.se.testing.learningtdd;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class FizzBuzzTest {
    private FizzBuzz game;

    @Before
    public void init() {
        game = new FizzBuzz();
    }

    @Test
    @Parameters(method = "shouldReturnTheGivenNumbersString_Parameters")
    public void shouldReturnTheGivenNumbersString(int n) {
        assertThat(game.play(n), is(equalTo(String.format("%d", n))));
    }

    private static Object[][] shouldReturnTheGivenNumbersString_Parameters() {
        return new Object[][] {
                {11}, {17}, {19}, {101}
        };
    }

    private static Object[][] shouldReturnFizz_Parameters() {
        return new Object[][] {
                {3}, {9}, {21}, {102}
        };
    }

    private static Object[][] shouldReturnBuzz_Parameters() {
        return new Object[][] {
                {5}, {25}, {100}, {1010}
        };
    }

    private static Object[][] shouldReturnFizzBuzz_Parameters() {
        return new Object[][] {
                {15}, {150}, {45}, {4515}
        };
    }

    @Test
    @Parameters(method = "shouldReturnFizz_Parameters")
    public void shouldReturnFizz(int n) {
        assertThat(game.play(n), is(equalTo("fizz")));
    }

    @Test
    @Parameters(method = "shouldReturnBuzz_Parameters")
    public void shouldReturnBuzz(int n) {
        assertThat(game.play(n), is(equalTo("buzz")));
    }

    @Test
    @Parameters(method = "shouldReturnFizzBuzz_Parameters")
    public void shouldReturnFizzBuzz(int n) {
        assertThat(game.play(n), is(equalTo("fizzbuzz")));
    }
}
