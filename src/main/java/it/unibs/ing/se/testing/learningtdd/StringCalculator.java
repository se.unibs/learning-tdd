package it.unibs.ing.se.testing.learningtdd;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringCalculator {
    public int add(String input) {
        if(input == null || input == "") {
            return 0;
        }
        String[] lines = input.split("[\n]");
        boolean customDelimiter = lines.length > 1 && lines[0].startsWith("//");
        String delimiter = customDelimiter
                ? lines[0].substring(2)
                : "[ \n]";

        String data = customDelimiter ? input.substring(input.indexOf('\n') + 1) : input;

        String[] tokens = data.split(delimiter);

        List<Integer> ints = Stream.of(tokens).map(Integer::parseInt).collect(Collectors.toList());
        Stream<Integer> negatives = ints.stream().filter(i -> i < 0);
        if(negatives.count() > 0) {
            throw new IllegalArgumentException("Can't handle negatives!");
        }
        return ints
            .stream()
            .reduce((acc, curr) -> acc + curr)
            .get();
    }
}
