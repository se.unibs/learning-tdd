package it.unibs.ing.se.testing.learningtdd;

public class Greeter {
    public String sayHello() {
        return "Hello, World!";
    }
}
