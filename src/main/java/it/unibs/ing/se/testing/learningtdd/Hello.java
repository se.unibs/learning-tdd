package it.unibs.ing.se.testing.learningtdd;

public class Hello {
    private final String message;

    public Hello(String message) {
        this.message = message;
    }

    public Hello() {
        this("Hello");
    }

    public String sayHello() {
        return sayHelloTo("World");
    }

    public String sayHelloTo(String to) {
        return String.format("%s, %s!", message, to);
    }
}
